//Number of matches played per year for all the years in IPL.
exports.matchesPlayedPerYear = (matchesData) => {
  if (!matchesData || matchesData.length === 0) return [];

  let matchesPlayedPerYear = matchesData.reduce((matchesPerYear, match) => {
    if (matchesPerYear[match.season] > 0) {
      matchesPerYear[match.season] += 1;
    } else {
      matchesPerYear[match.season] = 1;
    }
    return matchesPerYear;
  }, {});
  return matchesPlayedPerYear;
};

//Number of matches won per team per year in IPL.
exports.matchesWonPerYear = (matchesData) => {
  if (!matchesData || matchesData.length === 0) return [];

  let wonPerYear = matchesData.reduce((matchesWon, match) => {
    if (!matchesWon.hasOwnProperty(match.season)) {
      matchesWon[match.season] = {};
    }
    if (!matchesWon[match.season].hasOwnProperty(match.team1)) {
      matchesWon[match.season][match.team1] = 0;
    }
    if (!matchesWon[match.season].hasOwnProperty(match.team2)) {
      matchesWon[match.season][match.team2] = 0;
    }
    if (matchesWon[match.season].hasOwnProperty(match.winner)) {
      matchesWon[match.season][match.winner] += 1;
    }

    return matchesWon;
  }, {});
  return wonPerYear;
};

//Extra runs conceded per team in the year 2016
exports.extraRuns = (deliveriesData, matchesData) => {
  if (
    !matchesData ||
    !deliveriesData ||
    deliveriesData.length === 0 ||
    matchesData.length === 0
  ) {
    return [];
  }

  let extraRuns = {};

  matches2016 = matchesData.filter((match) => match.season === "2016");

  for (let match of matches2016) {
    for (let delivery of deliveriesData) {
      if (match.id === delivery.match_id) {
        if (!extraRuns.hasOwnProperty(delivery.bowling_team)) {
          extraRuns[delivery.bowling_team] = 0;
        }
        extraRuns[delivery.bowling_team] += parseInt(delivery.extra_runs);
      }
    }
  }
  return extraRuns;
};

// Top 10 economical bowlers in the year 2015
exports.topEcoBowlers = (deliveriesData, matchesData) => {
  if (
    !matchesData ||
    !deliveriesData ||
    deliveriesData.length === 0 ||
    matchesData.length === 0
  ) {
    return [];
  }

  let economicBowlers = [];
  let deliveries2015 = [];
  let bolwersOvers = {};

  let matches2015 = matchesData.filter((match) => match.season === "2015");

  //filtering 2015 deliveries data
  for (let match of matches2015) {
    for (let delivery of deliveriesData) {
      if (match.id === delivery.match_id) {
        if (!bolwersOvers.hasOwnProperty(delivery.bowling_team)) {
          bolwersOvers[delivery.bowling_team] = {};
        }
        deliveries2015.push(delivery);
      }
    }
  }

  //reducing deliveries array to get team and their bowlers
  deliveries2015.reduce((teams, delivery) => {
    if (!teams[delivery.bowling_team].hasOwnProperty(delivery.bowler)) {
      teams[delivery.bowling_team][delivery.bowler] = { runs: 0, bowls: 0 };
    }
    if (delivery.wide_runs === "0" && delivery.noball_runs === "0") {
      teams[delivery.bowling_team][delivery.bowler].bowls += 1;
    }
    teams[delivery.bowling_team][delivery.bowler].runs +=
      parseInt(delivery.noball_runs) +
      parseInt(delivery.wide_runs) +
      parseInt(delivery.batsman_runs);
    return teams;
  }, bolwersOvers);

  //calculating economy of all bowlers
  for (let team in bolwersOvers) {
    for (let bowler in bolwersOvers[team]) {
      let economyCalculation =
        bolwersOvers[team][bowler].runs /
        (bolwersOvers[team][bowler].bowls / 6);
      economicBowlers.push([bowler, economyCalculation.toFixed(2)]);
    }
  }

  //sorting economicbowlers to get top-10
  let topTen = economicBowlers
    .sort((a, b) => {
      let fa = a[1] * 100,
        fb = b[1] * 100;
      if (parseInt(fa) < parseInt(fb)) {
        return -1;
      } else if (parseInt(fa) > parseInt(fb)) {
        return 1;
      }
    })
    .slice(0, 10);
  return topTen;
};

exports.WinWinTeam = (matchesData, deliveriesData) => {
  console.log(matchesData);
  let winWinTeam = {};
  for (let match of matchesData) {
    if (!winWinTeam.hasOwnProperty(match.team1)) {
      winWinTeam[match.team1] = 0;
    }
    if (!winWinTeam.hasOwnProperty(match.team2)) {
      winWinTeam[match.team2] = 0;
    }
    if (match.winner === match.toss_winner) {
      winWinTeam[match.winner] += 1;
    }
  }
  return winWinTeam;
};

exports.playerOfTheMatch = (matchesData) => {
  let playerOfMatchBySeason = {};
  let manOfTheMatchBySeason = [];

  for (let match of matchesData) {
    if (!playerOfMatchBySeason.hasOwnProperty(match.season)) {
      playerOfMatchBySeason[match.season] = {};
    }
    if (
      playerOfMatchBySeason[match.season].hasOwnProperty(match.player_of_match)
    ) {
      playerOfMatchBySeason[match.season][match.player_of_match] += 1;
    } else {
      playerOfMatchBySeason[match.season][match.player_of_match] = 1;
    }
  }
  for (let season in playerOfMatchBySeason) {
    let playerName,
      max = 0;
    for (let player in playerOfMatchBySeason[season]) {
      if (max < playerOfMatchBySeason[season][player]) {
        max = playerOfMatchBySeason[season][player];
        playerName = player;
      }
    }
    manOfTheMatchBySeason.push({ season, playerName, max });
  }
  return manOfTheMatchBySeason;
};

exports.strikeRate = (matchesData, deliveriesData) => {
  let strikeRates = {};
  for (let match of matchesData) {
    for (let delivery of deliveriesData) {
      if (match.id === delivery.match_id) {
        if (!strikeRates.hasOwnProperty(match.season)) {
          strikeRates[match.season] = {};
        }
        if (!strikeRates[match.season].hasOwnProperty(delivery.batsman)) {
          strikeRates[match.season][delivery.batsman] = { balls: 0, runs: 0 };
        }
        if (delivery.wide_runs === "0" && delivery.noball_runs === "0") {
          strikeRates[match.season][delivery.batsman].balls += 1;
        }
        strikeRates[match.season][delivery.batsman].runs += parseInt(
          delivery.batsman_runs
        );
      }
    }
  }
  for (let season in strikeRates) {
    for (let batsman in strikeRates[season]) {
      strikeRates[season][batsman].rate = (
        (strikeRates[season][batsman].runs /
          strikeRates[season][batsman].balls) *
        100
      ).toFixed(2);
    }
  }
  return strikeRates;
};

exports.dismissedPlayers = (deliveriesData) => {
  let totalDismissedRate = {};
  let baleBaaj,
    geendBaaj,
    megaMax = 0;

  for (let delivery of deliveriesData) {
    if (delivery.player_dismissed.length > 0) {
      if (!totalDismissedRate.hasOwnProperty(delivery.batsman)) {
        totalDismissedRate[delivery.batsman] = {};
      }
      if (
        !totalDismissedRate[delivery.batsman].hasOwnProperty(delivery.bowler)
      ) {
        totalDismissedRate[delivery.batsman][delivery.bowler] = 0;
      }
      totalDismissedRate[delivery.batsman][delivery.bowler] += 1;
    }
  }
  for (let batsman in totalDismissedRate) {
    let max = 0,
      batsmanName,
      bowlerName;
    for (let bowler in totalDismissedRate[batsman]) {
      if (max < totalDismissedRate[batsman][bowler]) {
        max = totalDismissedRate[batsman][bowler];
        bowlerName = bowler;
        batsmanName = batsman;
      }
    }
    if (megaMax < max) {
      megaMax = max;
      baleBaaj = batsmanName;
      geendBaaj = bowlerName;
    }
  }
  return { totalwickets: megaMax, batsman: baleBaaj, bowler: geendBaaj };
};

exports.superOver = (deliversData) => {
  let superOverBowlers = {};
  for (delivery of deliversData) {
    if (parseInt(delivery.is_super_over) > 0) {
      if (!superOverBowlers.hasOwnProperty(delivery.bowler)) {
        superOverBowlers[delivery.bowler] = { runs: 0, balls: 0 };
      }
      if (delivery.wide_runs === "0" && delivery.noball_runs === "0") {
        superOverBowlers[delivery.bowler].balls += 1;
      }
      superOverBowlers[delivery.bowler].runs +=
        parseInt(delivery.noball_runs) +
        parseInt(delivery.wide_runs) +
        parseInt(delivery.batsman_runs);
    }
  }
  for (let bowler in superOverBowlers) {
    let maxEco = 0, bowlerName;
    let over = (
      Math.floor(superOverBowlers[bowler].balls / 6) +
      (superOverBowlers[bowler].balls % 6) * 0.1
    ).toFixed(1);
    let eco = (superOverBowlers[bowler].runs / parseFloat(over)).toFixed(2);
    superOverBowlers[bowler].economyrate = eco;

    if(maxEco < eco){
      maxEco = eco;
      bowlerName = bowler;
    }

  }
  console.log(superOverBowlers);
};
